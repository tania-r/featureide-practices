package eVotingApplication;

//A�ADIR ANOTACIONES PARA INCLUIR SOLO LAS LIBRER�AS NECESARIAS - logging 
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Authentication{
	
	//A�ADIR ANOTACIONES PARA ADAPTAR LOS M�TODOS DE ESTA CLASE - autenticaci�n
	public static boolean fingerPrintAuthentication() {
		println("Finger Print Authentication ... ");
		return Authentication.authentication();
	}

	public static boolean digitalCertificateAuthentication() {
		println("Digital Certificate Authentication ... ");
		return Authentication.authentication();
	}

	public static boolean votingKeyAuthentication() {
		println("Voting Key Authentication ... ");
		return Authentication.authentication();
	}

	private static boolean authentication() {
		boolean authenticated;
		
		int auth =  new java.util.Random().nextInt(2);
		if (auth == 1) {
			authenticated = true;
		}else {
			authenticated = false;
		}
		return authenticated;
	}
	
	private static void println(Object obj) {
		System.out.println(obj);
		
		//A�ADIR ANOTACIONES PARA ADAPTAR LA INFORMACI�N EN LA PANTALLA - logging 
		try(FileWriter fw = new FileWriter("LoggingFile.txt", true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw))
		{
			pw.println(obj);
		}catch(IOException e) {
			System.err.println("Error opening the logging file");
		}
	}
}
