package eVotingApplication;

public class Encryption {

	//A�ADIR ANOTACIONES PARA ADAPTAR LOS M�TODOS DISPONIBLES - encryption 
	public static Vote AESEncryption(String dni, String vote) {
		return new Vote(dni,"AESEncrypted");
	}
	
	public static Vote RSAEncryption(String dni, String vote) {
		return new Vote(dni,"RSAEncrypted");
	}
	
	public static Vote DESEncryption(String dni, String vote) {
		return new Vote(dni,"DESEncrypted");
	}
}