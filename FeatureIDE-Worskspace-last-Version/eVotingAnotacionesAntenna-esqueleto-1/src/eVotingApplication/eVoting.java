package eVotingApplication;

//A�ADIR ANOTACIONES PARA INCLUIR SOLO LAS LIBRER�AS NECESARIAS - logging 
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.io.IOException;


public class eVoting{
	private String dni;
	private String vote;
	private String authMechanism;
	private String encMechanism;

	
   public eVoting(String dni, String vote, String authMechanism, String encMechanism) {
		this.dni = dni;
		this.vote = vote;
		this.authMechanism = authMechanism;
		this.encMechanism = encMechanism;
	}
	
	private void println(Object obj) {
		System.out.println(obj);
		//A�ADIR ANOTACIONES PARA ADAPTAR LA INFORMACI�N EN LA PANTALLA - logging 
		try(FileWriter fw = new FileWriter("LoggingFile.txt", true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw))
		{
			pw.println(obj);
		}catch(IOException e) {
			System.err.println("Error opening the logging file");
		}
	}
	
	public void vote() {
		//A�ADIR ANOTACIONES PARA ADAPTAR EL C�DIGO AL TIPO DE DISPOSITIVO - mobileDevice, ... 
		if (authMechanism.equals("votingKey")){
			if (Authentication.votingKeyAuthentication()) {
				println("User correctly authenticated...");
				Vote encryptedVote = encryptVote(encMechanism,dni,vote);
				println(encryptedVote);
			}else {
				println("Invalid user ...");
			}
		}
		else if (authMechanism.equals("fingerPrint")){
			if (Authentication.fingerPrintAuthentication()) {
				println("User correctly authenticated...");
				Vote encryptedVote = encryptVote(encMechanism,dni,vote);
				println(encryptedVote);
			}else {
				println("Invalid user ...");
			}
		}
	    else if (authMechanism.equals("digitalCertificate")) { 
	    	if (Authentication.digitalCertificateAuthentication()) {
				println("User correctly authenticated...");
				Vote encryptedVote = encryptVote(encMechanism,dni,vote);
				println(encryptedVote);
			}else {
				println("Invalid user ...");
			}		
		}
	}
	
	private Vote encryptVote(String encMechanism, String dni, String vote) {
		Vote v = new Vote(dni,vote);
		
		//A�ADIR ANOTACIONES PARA ADAPTAR EL C�DIGO AL TIPO DE DISPOSITIVO - mobileDevice, ... 
		if (encMechanism.equals("AES")) {
			v = Encryption.AESEncryption(dni,vote);
		}
		else if (encMechanism.equals("RSA")) {
			v = Encryption.RSAEncryption(dni,vote);
		}else if (encMechanism.equals("DES")){
			v = Encryption.DESEncryption(dni,vote);
		}
		
		return v;
	}
	
}

class Vote{
	private String dni;
	private String vote;
	
	public Vote(String dni, String vote) {
		this.dni = dni;
		this.vote = vote;
	}
	
	public String toString() {
		return "(" + dni+","+ vote+")";
	}
}
