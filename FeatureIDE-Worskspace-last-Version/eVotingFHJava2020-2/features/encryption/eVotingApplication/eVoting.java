package eVotingApplication;

public class eVoting{
	
	private Vote encryption(String encMechanism, String dni, String vote) {
		return new Vote(dni,vote);
	}
	
	public boolean vote() {
		boolean value = original();
		if (value) {
			Vote encryptedVote = encryption(encMechanism,dni,vote);
			println(encryptedVote);
			return true;
		}else return false;
	}
}
