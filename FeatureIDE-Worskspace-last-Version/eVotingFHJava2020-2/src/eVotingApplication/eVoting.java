package eVotingApplication; 

public   class  eVoting {
	
	private String dni;

	
	private String vote;

	
	private String authMechanism;

	
	private String encMechanism;

	

   public eVoting(String dni, String vote, String authMechanism, String encMechanism) {
		this.dni = dni;
		this.vote = vote;
		this.authMechanism = authMechanism;
		this.encMechanism = encMechanism;
	}

	
	
	private void println(Object obj) {
		System.out.println(obj);
	}

	
	
	 private boolean  vote__wrappee__authentication  () {
		if (authenticated(authMechanism)) {
			println("User correctly authenticated...");
			return true;
		}else {
			return false;
		}
	}

	
	
	public boolean vote() {
		boolean value = vote__wrappee__authentication();
		if (value) {
			Vote encryptedVote = encryption(encMechanism,dni,vote);
			println(encryptedVote);
			return true;
		}else return false;
	}

	
	
	 private boolean  authenticated__wrappee__authentication  (String authMechanism) {
		return false;
	}

	
	
	 private boolean  authenticated__wrappee__digitalCertificate  (String authMechanism) {
		if (authMechanism.equals("digitalCertificate")) {
			return Authentication.digitalCertificateAuthentication();
		}
		return authenticated__wrappee__authentication(authMechanism);
	}

	
	
	private boolean authenticated(String authMechanism) {
		if (authMechanism.equals("votingKey")) {
			return Authentication.votingKeyAuthentication();
		}
		return authenticated__wrappee__digitalCertificate(authMechanism);
	}

	
	
	 private Vote  encryption__wrappee__encryption  (String encMechanism, String dni, String vote) {
		return new Vote(dni,vote);
	}

	
	
	private Vote encryption(String encMechanism, String dni, String vote) {
		if (encMechanism.equals("RSA")) {
			return Encryption.RSAEncryption(dni,vote);
		}
		return encryption__wrappee__encryption(encMechanism,dni,vote);
	}


}
