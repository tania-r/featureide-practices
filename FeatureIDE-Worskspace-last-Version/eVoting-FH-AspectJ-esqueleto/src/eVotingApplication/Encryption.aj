package eVotingApplication;

public aspect Encryption {
	

pointcut votePC(eVoting eV):call(public * eVoting.vote(..)) && target(eV);
	after(eVoting eV):votePC(eV){
		String encry = eV.getEncMechanism();
		String dni = eV.getDni();
		String vote = eV.getVote();
		authenticated(encry, dni, vote);
	}

	private void authenticated(String authMechanism, String dni, String vote) {
		String dni1 = dni;
		String vote1 = vote;
		if (authMechanism.equals("RSA")) {
			RSAEncryption(dni1, vote1);
		}else if (authMechanism.equals("AES")) {
			AESEncryption(dni1, vote1);
		}else if (authMechanism.equals("DES")){
			DESEncryption(dni1, vote1);
		}
	}
	
	
	public Vote RSAEncryption(String dni, String vote){
		System.out.println("dni: "+ dni + "vote: "+vote + "RSAEncrypted");
		System.out.println("dni: "+ dni + "vote: "+vote + " RSAEncrypted");
		return new Vote(dni,"RSAEncrypted");
	}
	
	public  Vote AESEncryption(String dni, String vote) {
		System.out.println("dni: "+ dni + "vote: "+vote + "AESEncrypted");
		System.out.println("dni: "+ dni + "vote: "+vote + " AESEncrypted");
		return new Vote(dni,"AESEncrypted");
	}
	
	public  Vote DESEncryption(String dni, String vote) {
		System.out.println("dni: "+ dni + "vote: "+vote + "DESEncrypted");
		System.out.println("dni: "+ dni + "vote: "+vote + " DESEncrypted");
		return new Vote(dni,"DESEncrypted");
	}
}
