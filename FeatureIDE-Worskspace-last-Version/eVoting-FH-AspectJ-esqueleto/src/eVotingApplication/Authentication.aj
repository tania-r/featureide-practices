package eVotingApplication;

public aspect Authentication{
	
	//Establecer precedencia entre los aspectos
	declare precedence: evotingPC+, votePC;
	
	pointcut evotingPC(eVoting eV):call(* eVoting.vote(..)) && target(eV);
	
	
	//Definir advice
	void around(eVoting eV):evotingPC(eV){
		String dni = eV.getDni();
		String vote = eV.getVote();
		String authentication = eV.getAuthMechanism();
		authenticated(authentication);
		
	}
	  
    
	private boolean authenticated(String authMechanism) {
		boolean auth = false;
		if (authMechanism.equals("digitalCertificate")) {
			auth = digitalCertificateAuthentication();
		}else if (authMechanism.equals("fingerPrint")) {
			auth = fingerPrintAuthentication();
		}else if (authMechanism.equals("votingKey")){
			auth = votingKeyAuthentication();
		}
		return auth;
	}
	
	public boolean fingerPrintAuthentication() {
		println("Finger Print Authentication ... ");
		return authentication();
	}
	
	public boolean digitalCertificateAuthentication() {
		println("Digital Certificate Authentication ... ");
		return authentication();
	}
	
	public boolean votingKeyAuthentication() {
		println("Voting Key Authentication ... ");
		return authentication();
	}
	
	//Este m�todo tendr� que ser interceptado por el aspecto de logging
	private void println(Object obj) {
		System.out.println(obj);
	}
	
	private boolean authentication() {
		boolean authenticated;
		
		int auth =  new java.util.Random().nextInt(2);
		if (auth == 1) {
			authenticated = true;
		}else {
			authenticated = false;
		}
		return authenticated;
	}
	
}
