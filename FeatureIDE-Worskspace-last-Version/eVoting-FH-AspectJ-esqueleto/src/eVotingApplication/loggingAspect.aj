package eVotingApplication;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public aspect loggingAspect {

	pointcut loggin(Object obj):
		call(* Authentication.println(*)) && target(obj);
	
	void around(Object obj):loggin(obj){
		System.out.println(obj);
		
		try( 	FileWriter fw = new FileWriter("LoggingFile.txt", true);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter pw = new PrintWriter(bw))
			{
				pw.println(obj);
			}catch(IOException e) {
				System.err.println("Error opening the logging file");
			}
		
	}
}


