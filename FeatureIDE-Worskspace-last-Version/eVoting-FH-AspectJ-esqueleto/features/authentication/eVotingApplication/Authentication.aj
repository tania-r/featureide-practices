package eVotingApplication;

public aspect Authentication {
	
	//Establecer precedencia entre los aspectos

	//Definir advice
	void around(eVoting eV): //definici�n punto de corte para interceptar como join point el metodo vote(){
		
	}
	
    /* Copiado directamente del c�digo de las versiones anteriores
       - Para simplificar asumimos que el aspecto incluye todo el c�digo independientemente del tipo de autenticaci�n disponible
       - Se puede implementar mejor haciendo que solo el c�digo de los tipos de autentication disponibles forme parte del producto
       - Problema: dependiendo de la herramienta no es posible o no es trivial usar varios tipos de "Composer" de forma simult�nea
       
       - Lo idea ser�a poder combinar varios enfoques.
       		-Ejemplo: Combinaci�n del enfoque composicional (compongo bloques de c�digo implementados 
       		          de forma independiente con Feature House y/o AspectJ) con el enfoque anotacional 
       		          (uso anotaciones para a�adir/quitar l�neas de c�digo dentro de un bloque).
    */   
    
	private boolean authenticated(String authMechanism) {
		boolean auth = false;
		if (authMechanism.equals("digitalCertificate")) {
			auth = digitalCertificateAuthentication();
		}else if (authMechanism.equals("fingerPrint")) {
			auth = fingerPrintAuthentication();
		}else if (authMechanism.equals("votingKey")){
			auth = votingKeyAuthentication();
		}
		return auth;
	}
	
	public boolean fingerPrintAuthentication() {
		println("Finger Print Authentication ... ");
		return authentication();
	}
	
	public boolean digitalCertificateAuthentication() {
		println("Digital Certificate Authentication ... ");
		return authentication();
	}
	
	public boolean votingKeyAuthentication() {
		println("Voting Key Authentication ... ");
		return authentication();
	}
	
	//Este m�todo tendr� que ser interceptado por el aspecto de logging
	private void println(Object obj) {
		System.out.println(obj);
	}
	
	private boolean authentication() {
		boolean authenticated;
		
		int auth =  new java.util.Random().nextInt(2);
		if (auth == 1) {
			authenticated = true;
		}else {
			authenticated = false;
		}
		return authenticated;
	}
	
}
